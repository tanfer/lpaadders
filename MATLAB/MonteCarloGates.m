% "Probability-Driven Evaluation of Lower-Part Approximation Adders" on IEEE TCAS II
%   Tanfer Alan @ CES - KIT Germany
%
%  Lower-part approximation adders binary behavioral models
%  -this script is called by the main file, MonteCarlo_RUN

function [ ea_trunc, ea_copy, ea_shiftcopy, ea_loa, ea_lxoa, ea_eta ] = monteCarloGatesAuto( abW )


bW = 36;    % Total Bit Width of the Adder including accurate and approximate parts
experiment_length = 1000;   % PARAMETER: Monte Carlo simulation steps / 100.000 in paper


 ea_trunc = 0;
 ea_copy = 0;
 ea_loa = 0;
 ea_lxoa = 0;
 ea_eta = 0;
 
for c = 1:experiment_length;
    OPa=(floor((2^bW)*rand));	%Generate Operands 1,2
    OPb=(floor((2^bW)*rand));
    OPa2=de2bi(OPa,bW);		%Binary represtation of operands
    OPb2=de2bi(OPb,bW);
    exactsum(c) = OPa+OPb;	%Exact addition

    truncsum(c) = bi2de(OPa2&[zeros(1,abW),ones(1,bW-abW)])+bi2de(OPb2&[zeros(1,abW),ones(1,bW-abW)]);
    copysum(c) = bi2de(de2bi(truncsum(c),bW+1)| [OPa2(1:abW),zeros(1,bW+1-abW)]);  % copy operand1
    shiftcopy(c) = bi2de(de2bi(truncsum(c),bW+1)| [1, OPa2(1:abW-1),zeros(1,bW+1-abW)]);  % shift_copy operand1


% LOA start    
temp_loa1 = OPa2(abW)&OPb2(abW); % LOA-to-Accurate Carry-in
temp_loa2 = [zeros(1,abW),temp_loa1,zeros(1,bW-abW-2)];
     loa(c) = bi2de(de2bi((truncsum(c)+bi2de(temp_loa2)),bW+1)| [OPa2(1:abW)|OPb2(1:abW),zeros(1,bW+1-abW)]);
% LOA end


% LXOA start
     trunc2 = bi2de(OPa2&[zeros(1,abW-1),ones(1,bW-abW+1)])+bi2de(OPb2&[zeros(1,abW-1),ones(1,bW-abW+1)]);
     trunc3 = de2bi(trunc2,bW+1);
     lxoa(c) = bi2de(trunc3 | [(OPa2(1:abW-1)|OPb2(1:abW-1)),zeros(1,bW+2-abW)]);
% LXOA end


% ETA start
     tempc = zeros(1,abW);
for ceta = 1:abW
    tempc(ceta) =OPa2(ceta) + OPb2(ceta);
    eta_con = (OPa2(ceta)&OPb2(ceta));
    tempc= (eta_con==1).*[ones(1,ceta), zeros(1,abW-ceta)] + ~(eta_con==1).*tempc ;
end
eta(c) = truncsum(c) + bi2de(tempc);
% ETA end

  
     error_trunc(c) = exactsum(c) - truncsum(c);
     error_copysum(c) = exactsum(c) - copysum(c);
     error_shiftcopy(c) = exactsum(c) - shiftcopy(c);
     error_loa(c) = exactsum(c) - loa(c);
     error_lxoa(c) = exactsum(c) - lxoa(c);
     error_eta(c) = exactsum(c) - eta(c);  

end % experiment_length

 ea_trunc = mean(mean(error_trunc));
 ea_copy = mean(mean(error_copysum));
 ea_shiftcopy = mean(mean(abs(error_shiftcopy)));
 ea_loa = mean(mean(abs(error_loa)));
 ea_lxoa = mean(mean(abs(error_lxoa)));
 ea_eta = mean(mean(abs(error_eta)));
 


end
