% "Probability-Driven Evaluation of Lower-Part Approximation Adders" on IEEE TCAS II
%   Tanfer Alan @ CES - KIT Germany
%
%  Main file for Monte-Carlo simulation of lower-part approximation adders
%  -requires the script: MonteCarloGates

function [ output_args ] = monteCarloRun( input_args )


tic  %Experiment Timer-Start
for abW = 1:16;   % PARAMETER: Approximate Bit-Width Sweep
[ ea_trunc(abW), ea_copy(abW), ea_shiftcopy(abW), ea_loa(abW), ea_lxoa(abW), ea_eta(abW) ] = MonteCarloGates( abW );
abW
end
toc  %Experiment Timer-Stop

figure; semilogy(ea_trunc);
hold on
semilogy(ea_copy);
semilogy(ea_shiftcopy);
semilogy(ea_loa);
semilogy(ea_eta);
semilogy(ea_lxoa);
grid on;

end

