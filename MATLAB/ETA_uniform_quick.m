% "Probability-Driven Evaluation of Lower-Part Approximation Adders" on IEEE TCAS II
%   Tanfer Alan @ CES - KIT Germany

function [ output_args ] = eta_uniform_quick( input_args )
% Expected Value of Error
% Model of Error Tolerant Adder (ETA) from
%N. Zhu, W. L. Goh, and K. S. Yeo, “An enhanced low-power high-speed adder
%for error-tolerant application,” in Integrated Circuits, ISIC’09. Proceedings of the
%2009 12th International Symposium on. IEEE, 2009, pp. 69–72.


k=16;
tic  %Experiment Timer-Start
for d = 1:k;
    temp(d) = (1/4)* ( ((3/4)^(k-d)) * 2^(d-1));
    
end
eta_error_uniform = sum(temp)
toc  %Experiment Timer-Stop
end

