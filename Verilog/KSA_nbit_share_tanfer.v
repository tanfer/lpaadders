/////////////////////////////////////////////////////////////////////
////                                                             ////
////  Kogge-Stone Adder (unsigned) - Verilog                     ////
////                                                             ////
////  Author: Tanfer Alan                                        ////
////          @ CES - KIT 2016                                   ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
//
// If you use the verilog code, please cite [1] or [2]:
// [1] Tanfer Alan and Jörg Henkel. "Slackhammer: Logic synthesis for graceful errors under frequency scaling." IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems 37.11 (2018): 2802-2811.
// [2] Tanfer Alan and Jörg Henkel. "Probability-Driven Evaluation of Lower-Part Approximation Adders" IEEE Transactions on Circuits and Systems II: Express Briefs (2021).
//----------------------------------------------------------------------------------------------------


module ks_nbits(A,B,S,Cout );
parameter bit_width = 4;
parameter radix = 2;

input wire [bit_width-1:0] A;
input wire [bit_width-1:0] B;
wire [bit_width-1:0] P,G,P2,G2,P3,G3;
wire [bit_width-1:0] Ci;
output wire [bit_width-1:0] S;
wire Cin = 0;
output wire Cout;
genvar c;
assign G = A&B;
assign P = A^B;

assign P2[0] = P[0];
assign G2[0] = G[0];
for (c = 1;  c <bit_width; c=c+1) begin
  assign P2[c] = P[c]&&P[c-1];
  assign G2[c] = (P[c]&&G[c-1])|G[c];
end


for (c = 0;  c <bit_width; c=c+1) begin
  assign P3[c] = P2[c]&((c-2)>=0 ? P2[c-2] : 1);
  assign G3[c] = (P2[c]&((c-2)>=0 ? G2[c-2] : 0))|G2[c];
end

assign Ci = G3;
assign S[0] = P[0]^Cin;
assign S[bit_width-1:1] = P[bit_width-1:1]^Ci[bit_width-2:0];
assign Cout = Ci[bit_width-1];
endmodule
