// Unsigned Carry-Lookahead Adder 32 Bit
// Accurate
//
// Tanfer Alan - 2016 KIT|CES Germany
//  
// If you use the verilog code, please cite:
// Tanfer Alan and Jörg Henkel. "Probability-Driven Evaluation of Lower-Part Approximation Adders" IEEE Transactions on Circuits and Systems II: Express Briefs (2021).
//----------------------------------------------------------------------------------------------------


module cla32(A,B,C0,S,C32)
    input [31:0] A,B;
    input C0;
    output [31:0] S;
    output C32;

    reg [31:0] S;
    reg [32:0] C;

    integer i;
    always @* begin
        C[0] <= C0;
        for (i=0; i<32; i=i+1) begin
            S[i] <= A[i] ^ B[i] ^ C[i];
            C[i+1] <= ( A[i] & B[i] ) | ( A[i] & C[i] ) | ( B[i] & C[i] );
        end
    end

    assign C32 = C[32];

endmodule
