// Unsigned Carry-Lookahead Adder 32 Bit
// Lower Part Approximation with LXOA, Albicocco@ASILOMAR2012
//
// Tanfer Alan - 2016 KIT|CES Germany
//  
// If you use the verilog code, please cite:
// Tanfer Alan and Jörg Henkel. "Probability-Driven Evaluation of Lower-Part Approximation Adders" IEEE Transactions on Circuits and Systems II: Express Briefs (2021).
//----------------------------------------------------------------------------------------------------


module cla32_lxoa(A,B,C0,S,C32);
  parameter k = 2;//PARAMETER: Approximate bits
    input [31:0] A,B;
    input C0;	//left-open
    output [31:0] S;
    output C32;

    reg [31:0] S;
    reg [32:0] C;

    integer i;
    always @* begin
        S[k-2:0] <= A[k-2:0]|B[k-2:0];  //Lower Part OR Gate - Approximation
        S[k-1] <= A[k-1]^B[k-1];        //Half-Adder
        C[k] <= A[k-1]&B[k-1];        //Half-Adder
        for (i=k; i<32; i=i+1) begin
            S[i] <= A[i] ^ B[i] ^ C[i];
            C[i+1] <= ( A[i] & B[i] ) | ( A[i] & C[i] ) | ( B[i] & C[i] );
        end
    end

    assign C32 = C[32];

endmodule
