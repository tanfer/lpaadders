// 32-bit Kogge-Stone Adder with  radixes 4-4-2 (for the final stage)
// Lower Part Approximation with LXOA, Albicocco@ASILOMAR2012
//
// Tanfer Alan - 2016 KIT|CES Germany
//  
// If you use the verilog code, please cite:
// Tanfer Alan and Jörg Henkel. "Probability-Driven Evaluation of Lower-Part Approximation Adders" IEEE Transactions on Circuits and Systems II: Express Briefs (2021).
//----------------------------------------------------------------------------------------------------


module ks32_lxoa(A_in,B_in,S,Cout );
parameter k = 2; //PARAMETER: Approximate bits
parameter in_width = 32;
parameter bit_width = 32-k;
parameter radix = 4;

input wire [in_width-1:0] A_in;
input wire [in_width-1:0] B_in;

output wire [in_width-1:0] S;

output wire Cout;

assign S[k-2:0] = A_in[k-2:0]|B_in[k-2:0];
assign S[k-1] = A_in[k-1]^B_in[k-1];
wire Cin = A_in[k-1]&B_in[k-1];
wire [bit_width-1:0] A = A_in[in_width-1:k];
wire [bit_width-1:0] B = B_in[in_width-1:k];


wire [bit_width-1:0] P,G,P2,G2,P3,G3,P4,G4;
wire [bit_width-1:0] Ci;
wire [bit_width-1:0] S_ks;
genvar c;
assign G = A&B;
assign P = A^B;


for (c = 0;  c <bit_width; c=c+1) begin
  assign P2[c] = P[c]&((c-1)>=0 ? P[c-1] : 1)&((c-2)>=0 ? P[c-2] : 1)&((c-3)>=0 ? P[c-3] : 1);
  assign G2[c] = (P[c]&(((c-1)>=0 ? G[c-1] : 0)|((c-2)>=0 ? P[c-1]&G[c-2] : 0)|((c-3)>=0 ? P[c-1]&P[c-2]&G[c-3] : 0)))|G[c];
end


for (c = 0;  c <bit_width; c=c+1) begin
  assign P3[c] = P2[c]&((c-4)>=0 ? P2[c-4] : 1)&((c-8)>=0 ? P2[c-8] : 1)&((c-12)>=0 ? P2[c-12] : 1);
  assign G3[c] = (P2[c]&(((c-4)>=0 ? G2[c-4] : 0)|((c-8)>=0 ? P2[c-4]&G2[c-8] : 0)|((c-12)>=0 ? P2[c-4]&P2[c-8]&G2[c-12] : 0)))|G2[c];
end

for (c = 0;  c <bit_width; c=c+1) begin
//  assign P4[c] = P3[c]&((c-16)>=0 ? P3[c-16] : 1)&((c-32)>=0 ? P3[c-32] : 1)&((c-48)>=0 ? P3[c-48] : 1);
  assign G4[c] = (P3[c]&(((c-16)>=0 ? G3[c-16] : 0)|((c-32)>=0 ? P3[c-16]&G3[c-32] : 0)|((c-48)>=0 ? P3[c-16]&P3[c-32]&G3[c-48] : 0)))|G3[c];
end

assign Ci = G4;
assign S_ks[0] = P[0]^Cin;
assign S_ks[bit_width-1:1] = P[bit_width-1:1]^Ci[bit_width-2:0];
assign Cout = Ci[bit_width-1];
assign S[in_width-1:k] = S_ks;
endmodule


